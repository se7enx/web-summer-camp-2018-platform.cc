# Local development with Platform.CC

## Set up eZ Platform on Platform.cc
### 0. Requirements
- Python 2.7
- Pip (`sudo apt install python-pip`)
- Docker
  - Mac:
     1.  Install [Version 17.09.1-ce-mac42](https://download.docker.com/mac/stable/21090/Docker.dmg). If you are unable to install that version - try to upgrade your OS.
     2.  Open Docker -> Preferences -> File Sharing and remove all directories except `/tmp`.
     3.  Open Docker -> Preferences -> Advanced and increase memory to 4.0 GB.
     4.  Install `d4m-nfs`:

         ```bash
         $ cd ~  
         $ git clone git@github.com:IFSight/d4m-nfs.git  
         $ cd d4m-nfs  
         $ echo /Users:/Users > etc/d4m-nfs-mounts.txt  
         $ sudo rm /etc/exports && sudo touch /etc/exports  
         $ ./d4m-nfs.sh
         ```
  - Linux:

    Follow the instructions here [https://github.com/docker/docker-install](https://github.com/docker/docker-install).
    You might have to add yourself to the docker user group:

    ```bash
    $ sudo addgroup --system docker
    $ sudo adduser $USER docker
    $ newgrp docker
    ```

    And once you have Docker, pull the latest PHP 7.2 image from our repo. This would be done automatically later, but doing it now saves us some time.

    ```bash
    $ docker pull registry.gitlab.com/contextualcode/platform_cc/php72-fpm
    ```

### 1. Install Platform.CC
```bash
$ cd ~
$ git clone https://gitlab.com/contextualcode/platform_cc.git
$ cd platform_cc
$ pip install -r requirements.txt
$ sudo python setup.py install
```
### 2. Clone your project repo
```bash
$ cd ~
$ git clone https://gitlab.com/contextualcode/ezplatform.git --recursive
$ cd ezplatform
```
### 3. Build and deploy project
```
$ platform_cc project:start
$ platform_cc app:deploy
```
### 4. Get your route
```bash
$ platform_cc project:routes
```
And it will show `$PROJECTID.*` as a route. This is a wildcard route.
Add `127.0.0.1 $PROJECTID.com` to your `/etc/hosts` file.
And now we can visit that url in our browser for local development!

And from now on,
```bash
$ platform_cc project:start
```
is all it should take to get the application running again.

### Setup Database
In this demo project, the deploy hook ran a one-time setup command (`php bin/console ezplatform:install`).
[https://gitlab.com/contextualcode/ezplatform/blob/master/.platform.app.yaml#L80-85](https://gitlab.com/contextualcode/ezplatform/blob/master/.platform.app.yaml#L80-85)

But in a real project, this isn’t realistic. So how do you import your database? After running `platform_cc project:start`, you can pull your database with the `platform` CLI and import it into Platform.CC like so:
```bash
$ platform db:dump -emaster -fdb_dump.sql
$ cat db_dump.sql | platform_cc mysql:sql -dmain
```

## More tips, info, and commands
### Adding routes
Say we wanted to add 2 new domains and routes to our `.platform/routes.yaml`. Let’s add:
```yaml
"https://example1.com":
  type: upstream
  upstream: "app:http"

"https://example2.com":
  type: upstream
  upstream: "app:http"
```
A project restart is necessary to pick up the new routes:
```bash
$ platform_cc project:restart
```
Run:
```bash
$ platform_cc project:routes
```
again, and notice we have some new development urls:
- https://example1.com
- https://example1.com.$PROJECTID.*
- https://example2.com
- https://example2.com.$PROJECTID.*

### Shell into application
If you want to to access your app’s docker container, you can shell directly into it with:
```bash
$ platform_cc app:shell
```
### Access database
To access your database:
```bash
$ platform_cc mysql:sql
```
### Project variables
Projects can have environment variables with the same syntax as EPC env varables (`env:TEST`).
```bash
$ platform_cc var:set 'env:CITY' 'Rovinj'
$ platform_cc var:get 'env:CITY' 
$ platform_cc project:restart
$ platform_cc app:shell
$ echo $CITY
```
You can also set non-environment variables with names like `TEST`.

This was unnecessary on the demo project, but might be necessary when setting up your own projects (in order to clone git submodules, composer install with credentials, etc.).
```bash
$ platform_cc var:set 'env:COMPOSER_AUTH' " `cat ~/.composer/auth.json | tr -d '\n\r '` "
$ platform_cc var:set project:ssh_key `cat ~/.ssh/id_rsa | base64 -w 0`
$ platform_cc var:set project:known_hosts `cat ~/.ssh/known_hosts | base64 -w 0`
```
(use `base64 -b 0` on Mac)
### Rebuilding application
If for any reason we want to rebuild the application (for example: we want to re-run composer, re-run build hooks, or re-build the docker image), we can run:
```bash
$ platform_cc app:build
```
(This was automatically done when we first ran `platform_cc project:start`.)

If a new application image is pushed to our repo, you can pull it down and rebuild:
```bash
$ platform_cc app:pull
$ platform_cc app:build
```
### Purge project
To completely delete the docker containers, volumes, and everything to do with the current Platform.CC project:
```bash
$ platform_cc project:purge
```
### Services
To see the services set up for your project:
```bash
$ platform_cc service:list
```
And if you want to shell into a service’s container:
```bash
$ platform_cc service:shell mysqldb
$ platform_cc service:shell rediscache
```
### Multiple Applications
A Platform.CC project supports having more than one application, just like EPC ([https://docs.platform.sh/configuration/app/multi-app.html](https://docs.platform.sh/configuration/app/multi-app.html)).

## Options
Platform.CC has a few options that control behavior.
To see all options:
```bash
$ platform_cc project:options
```
To change an option:
```bash
$ platform_cc project:option_set OPTION_NAME [true|false]
```
### Mount Volumes
This option (`USE_MOUNT_VOLUMES`) is disabled by default. If you enable it, it will use Docker volumes for application mount volumes. 
Setting this to `true` is very important for speed on Macs.
### Crons
Crons are disabled by default. You can enable them by setting `ENABLE_CRON` to `true`.
## Custom config
If you find that you need some variables that are specific only to your Platform.CC projects, you can put those in a file called `.platform.app.pcc.yaml`. This should be in the same format as your `.platform.app.yaml` file.

For example, if you wanted to have the environment variable `$SYMFONY_ENV` set to `dev`, you could set it with `var:set`:
```bash
$ platform_cc var:set 'env:SYMFONY_ENV' 'dev'
```
But this would have to be ran each time you restarted the project. If you wanted `$SYMFONY_ENV` to always be `dev` when in Platform.CC, you can create `.platform.app.pcc.yaml` file with contents:
```yaml
variables:
    env:
        SYMFONY_ENV: dev
```
In this way, you can have variables and settings that are only and automatically set in your local development environments. And importantly, it uses the same syntax as your `.platform.app.yaml` files.

